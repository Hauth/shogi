using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Shogi.UI;
using Shogi.UI.Identity;
using Shogi.UI.Shared;
using System.Text.Json;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");
builder.Logging.AddConfiguration(
		builder.Configuration.GetSection("Logging"));
ConfigureDependencies(builder.Services, builder.Configuration);

builder.Services.AddResponseCompression(options =>
{
	options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(["application/octet-stream"]);
});

await builder.Build().RunAsync();

static void ConfigureDependencies(IServiceCollection services, IConfiguration configuration)
{
	/**
 * Why two HTTP clients?
 * See qhttps://docs.microsoft.com/en-us/aspnet/core/blazor/security/webassembly/additional-scenarios?source=recommendations&view=aspnetcore-6.0#unauthenticated-or-unauthorized-web-api-requests-in-an-app-with-a-secure-default-client
 */
	var baseUrl = configuration["ShogiApiUrl"];
	if (string.IsNullOrWhiteSpace(baseUrl))
	{
		throw new InvalidOperationException("ShogiApiUrl configuration is missing.");
	}

	var shogiApiUrl = new Uri(baseUrl, UriKind.Absolute);

	services
		.AddTransient<CookieCredentialsMessageHandler>()
		.AddTransient<ILocalStorage, LocalStorage>();

	// Identity
	services
		.AddAuthorizationCore(options => options.AddPolicy("Admin", policy => policy.RequireUserName("Hauth@live.com")))
		.AddScoped<AuthenticationStateProvider, CookieAuthenticationStateProvider>()
		.AddScoped(sp => (IAccountManagement)sp.GetRequiredService<AuthenticationStateProvider>())
		.AddHttpClient("Auth", client => client.BaseAddress = shogiApiUrl) // "Auth" is the name expected by the auth library.
		.AddHttpMessageHandler<CookieCredentialsMessageHandler>();

	// Network clients
	services
		.AddHttpClient<ShogiApi>(client => client.BaseAddress = shogiApiUrl)
		.AddHttpMessageHandler<CookieCredentialsMessageHandler>();
	services
		.AddSingleton<GameHubNode>();


	var serializerOptions = new JsonSerializerOptions
	{
		WriteIndented = true
	};
	services.AddSingleton((sp) => serializerOptions);
}