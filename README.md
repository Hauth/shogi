# Shogi
A web application for playing the Shogi boardgame with others, developed as a hobby and hosted at [https://lucaserver.space/shogi/](https://lucaserver.space/shogi/)  
The application uses sockets to allow players to enjoy sessions in real time.

### Technologies used
A Blazor UI backed by an Asp.net Core API service which uses Sql Server for presistent storage.

### Known Issues
 * The app is intended to support logging in via Microsoft accounts or browser-session (Guest) accounts, but currently Microsoft login does not work.
     * The workaround is to use the guest login.
 * On first load of the UI, guest account login will fail.
     * The workaround is to refresh the page and try again. This issue only happens on first load.


### Roadmap of features remaining
The app is not yet finished, though much of the functionality exists. Here is a list of what remains.

 * Placing pieces from the hand onto the board.
 * Checkmate experience in UI
 * Preventing the rarely invoked rule where check-mate cannot be gained by placing a pawn from the hand.
 * Retaining an archive of games played and move history of each game.
 * Adaptive UI layout for varying viewport (screen) sizes.