﻿using Shogi.Domain.ValueObjects;

namespace Shogi.Domain.YetToBeAssimilatedIntoDDD
{
    internal static class DomainExtensions
    {
        public static bool IsKing(this Piece self) => self.WhichPiece == WhichPiece.King;

        public static bool IsBetween(this float self, float min, float max)
        {
            return self >= min && self <= max;
        }

        public static bool IsInsideBoardBoundary(this Vector2 self)
        {
            return self.X.IsBetween(0, 8) && self.Y.IsBetween(0, 8);
        }
    }
}
