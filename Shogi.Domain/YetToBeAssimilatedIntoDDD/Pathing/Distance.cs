﻿namespace Shogi.Domain.YetToBeAssimilatedIntoDDD.Pathing;

public enum Distance
{
	/// <summary>
	/// Signifies that a piece can move one tile/position per move.
	/// </summary>
	OneStep,
	/// <summary>
	/// Signifies that a piece can move multiple tiles/positions in a single move.
	/// </summary>
	MultiStep
}