﻿namespace Shogi.Domain.YetToBeAssimilatedIntoDDD.Pathing;

public static class Direction
  {
      public static readonly Vector2 Forward = new(0, 1);
      public static readonly Vector2 Backward = new(0, -1);
      public static readonly Vector2 Left = new(-1, 0);
      public static readonly Vector2 Right = new(1, 0);
      public static readonly Vector2 ForwardLeft = new(-1, 1);
      public static readonly Vector2 ForwardRight = new(1, 1);
      public static readonly Vector2 BackwardLeft = new(-1, -1);
      public static readonly Vector2 BackwardRight = new(1, -1);
      public static readonly Vector2 KnightLeft = new(-1, 2);
      public static readonly Vector2 KnightRight = new(1, 2);
  }
