﻿using System.Diagnostics;

namespace Shogi.Domain.YetToBeAssimilatedIntoDDD.Pathing;

[DebuggerDisplay("{Step} - {Distance}")]
public record Path
{
	public Vector2 Step { get; }
	public Vector2 NormalizedStep => Vector2.Normalize(Step);
	public Distance Distance { get; }

	/// <summary>
	/// 
	/// </summary>
	/// <param name="step">The smallest distance that can occur during a move.</param>
	/// <param name="distance"></param>
	public Path(Vector2 step, Distance distance = Distance.OneStep)
	{
		Step = step;
		this.Distance = distance;
	}

	public Path Invert() => new(Vector2.Negate(Step), Distance);
}