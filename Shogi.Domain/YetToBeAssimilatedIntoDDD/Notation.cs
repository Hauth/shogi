﻿using System.Text.RegularExpressions;

namespace Shogi.Domain.YetToBeAssimilatedIntoDDD
{
    public static class Notation
    {
        private static readonly string BoardNotationRegex = @"(?<file>[A-I])(?<rank>[1-9])";
        private static readonly char A = 'A';

        public static string ToBoardNotation(Vector2 vector)
        {
            return ToBoardNotation((int)vector.X, (int)vector.Y);
        }

        public static string ToBoardNotation(int x, int y)
        {
            var file = (char)(x + A);
            var rank = y + 1;
            return $"{file}{rank}";
        }
        public static Vector2 FromBoardNotation(string notation)
        {
            if (Regex.IsMatch(notation, BoardNotationRegex))
            {
                var match = Regex.Match(notation, BoardNotationRegex, RegexOptions.IgnoreCase);
                char file = match.Groups["file"].Value[0];
                int rank = int.Parse(match.Groups["rank"].Value);
                return new Vector2(file - A, rank - 1);
            }
            throw new ArgumentException($"Board notation not recognized. Notation given: {notation}");
        }
    }
}
