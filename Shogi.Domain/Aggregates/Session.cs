﻿using Shogi.Domain.ValueObjects;

namespace Shogi.Domain.Aggregates;

public class Session(Guid id, string player1Name)
{
	public Guid Id { get; } = id;

	public ShogiBoard Board { get; } = new(BoardState.StandardStarting);

	/// <summary>
	/// The email of the player which created the session.
	/// </summary>
	public string Player1 { get; } = player1Name;

	/// <summary>
	/// The email of the second player.
	/// </summary>
	public string? Player2 { get; private set; }

	public void AddPlayer2(string player2Name)
	{
		if (this.Player2 != null) throw new InvalidOperationException("Player 2 already exists while trying to add a second player.");
		if (this.Player1.Equals(player2Name, StringComparison.OrdinalIgnoreCase)) throw new InvalidOperationException("Player 2 must be different from Player 1");
		this.Player2 = player2Name;
	}

	public bool IsSeated(string playerName)
	{
		return this.Player1 == playerName || this.Player2 == playerName;
	}
}
