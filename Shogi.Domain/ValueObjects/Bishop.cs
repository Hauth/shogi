﻿using Shogi.Domain.YetToBeAssimilatedIntoDDD.Pathing;
using System.Collections.ObjectModel;

namespace Shogi.Domain.ValueObjects
{
    internal record class Bishop : Piece
    {
        private static readonly ReadOnlyCollection<Path> BishopPaths = new(new List<Path>(4)
        {
            new Path(Direction.ForwardLeft, Distance.MultiStep),
            new Path(Direction.ForwardRight, Distance.MultiStep),
            new Path(Direction.BackwardLeft, Distance.MultiStep),
            new Path(Direction.BackwardRight, Distance.MultiStep)
        });

        public static readonly ReadOnlyCollection<Path> PromotedBishopPaths = new(new List<Path>(8)
        {
            new Path(Direction.Forward),
            new Path(Direction.Left),
            new Path(Direction.Right),
            new Path(Direction.Backward),
            new Path(Direction.ForwardLeft, Distance.MultiStep),
            new Path(Direction.ForwardRight, Distance.MultiStep),
            new Path(Direction.BackwardLeft, Distance.MultiStep),
            new Path(Direction.BackwardRight, Distance.MultiStep)
        });

        public static readonly ReadOnlyCollection<Path> Player2Paths =
            BishopPaths
            .Select(p => p.Invert())
            .ToList()
            .AsReadOnly();

        public static readonly ReadOnlyCollection<Path> Player2PromotedPaths =
            PromotedBishopPaths
            .Select(p => p.Invert())
            .ToList()
            .AsReadOnly();

        public Bishop(WhichPlayer owner, bool isPromoted = false)
            : base(WhichPiece.Bishop, owner, isPromoted)
        {
        }

        public override IEnumerable<Path> MoveSet => IsPromoted ? PromotedBishopPaths : BishopPaths;
    }
}
