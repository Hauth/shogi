﻿using Shogi.Domain.YetToBeAssimilatedIntoDDD.Pathing;
using System.Collections.ObjectModel;

namespace Shogi.Domain.ValueObjects;

internal record class King : Piece
{
	private static readonly ReadOnlyCollection<Path> Player1Paths = new(
			[
					new Path(Direction.Forward),
					new Path(Direction.Left),
					new Path(Direction.Right),
					new Path(Direction.Backward),
					new Path(Direction.ForwardLeft),
					new Path(Direction.ForwardRight),
					new Path(Direction.BackwardLeft),
					new Path(Direction.BackwardRight)
			]);

	private static readonly ReadOnlyCollection<Path> Player2Paths = Player1Paths.Select(p => p.Invert()).ToList().AsReadOnly();

	public King(WhichPlayer owner, bool isPromoted = false)
			: base(WhichPiece.King, owner, isPromoted)
	{
	}

	public override IEnumerable<Path> MoveSet => Owner == WhichPlayer.Player1 ? Player1Paths : Player2Paths;


}
