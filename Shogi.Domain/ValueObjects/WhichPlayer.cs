﻿namespace Shogi.Domain.ValueObjects
{
    public enum WhichPlayer
    {
        Player1,
        Player2
    }
}
