﻿using Shogi.Domain.YetToBeAssimilatedIntoDDD.Pathing;
using System.Collections.ObjectModel;

namespace Shogi.Domain.ValueObjects
{
    internal record class SilverGeneral : Piece
    {
        public static readonly ReadOnlyCollection<Path> Player1Paths = new(new List<Path>(4)
        {
            new Path(Direction.Forward),
            new Path(Direction.ForwardLeft),
            new Path(Direction.ForwardRight),
            new Path(Direction.BackwardLeft),
            new Path(Direction.BackwardRight)
        });

        public static readonly ReadOnlyCollection<Path> Player2Paths =
            Player1Paths
            .Select(p => p.Invert())
            .ToList()
            .AsReadOnly();

        public SilverGeneral(WhichPlayer owner, bool isPromoted = false)
            : base(WhichPiece.SilverGeneral, owner, isPromoted)
        {
        }

        public override ReadOnlyCollection<Path> MoveSet => Owner switch
        {
            WhichPlayer.Player1 => IsPromoted ? GoldGeneral.Player1Paths : Player1Paths,
            WhichPlayer.Player2 => IsPromoted ? GoldGeneral.Player2Paths : Player2Paths,
            _ => throw new NotImplementedException(),
        };
    }
}
