﻿namespace Shogi.Domain.ValueObjects;

/// <summary>
/// Represents a single piece being moved by a player from <paramref name="From"/> to <paramref name="To"/>.
/// </summary>
public readonly record struct Move
{
    public Move(Vector2 from, Vector2 to)
    {
        From = from;
        To = to;
    }
    public Move(WhichPiece pieceFromHand, Vector2 to)
    {
        PieceFromHand = pieceFromHand;
        To = to;
    }

    public Vector2? From { get; }
    public Vector2 To { get; }
    public WhichPiece PieceFromHand { get; }
}
