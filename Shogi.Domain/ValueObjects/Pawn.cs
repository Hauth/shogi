﻿using Shogi.Domain.YetToBeAssimilatedIntoDDD.Pathing;
using System.Collections.ObjectModel;

namespace Shogi.Domain.ValueObjects
{
    internal record class Pawn : Piece
    {
        public static readonly ReadOnlyCollection<Path> Player1Paths = new(new List<Path>(1)
        {
            new Path(Direction.Forward)
        });

        public static readonly ReadOnlyCollection<Path> Player2Paths =
            Player1Paths
            .Select(p => p.Invert())
            .ToList()
            .AsReadOnly();

        public Pawn(WhichPlayer owner, bool isPromoted = false)
            : base(WhichPiece.Pawn, owner, isPromoted)
        {
        }

        public override ReadOnlyCollection<Path> MoveSet => Owner switch
        {
            WhichPlayer.Player1 => IsPromoted ? GoldGeneral.Player1Paths : Player1Paths,
            WhichPlayer.Player2 => IsPromoted ? GoldGeneral.Player2Paths : Player2Paths,
            _ => throw new NotImplementedException(),
        };
    }
}
