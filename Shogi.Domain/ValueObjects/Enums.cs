﻿namespace Shogi.Domain.ValueObjects;

[Flags]
internal enum InCheckResult
{
	NobodyInCheck = 1, // This kinda doesn't make sense from a Flags perspective, but it works. =/
	Player1InCheck = 2,
	Player2InCheck = 4
}

public enum GameOverResult
{
	GameIsNotOver,
	Player1Wins,
	Player2Wins
}

public enum WhichPiece
{
	King,
	GoldGeneral,
	SilverGeneral,
	//PromotedSilverGeneral,
	Bishop,
	//PromotedBishop,
	Rook,
	//PromotedRook,
	Knight,
	//PromotedKnight,
	Lance,
	//PromotedLance,
	Pawn,
	//PromotedPawn,
}
