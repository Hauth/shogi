﻿using Shogi.Domain.YetToBeAssimilatedIntoDDD.Pathing;
using System.Collections.ObjectModel;

namespace Shogi.Domain.ValueObjects;

public record class Rook : Piece
{
  public static readonly ReadOnlyCollection<Path> Player1Paths = new(new List<Path>(4)
      {
          new Path(Direction.Forward, Distance.MultiStep),
          new Path(Direction.Left, Distance.MultiStep),
          new Path(Direction.Right, Distance.MultiStep),
          new Path(Direction.Backward, Distance.MultiStep)
      });

  private static readonly ReadOnlyCollection<Path> PromotedPlayer1Paths = new(new List<Path>(8)
      {
          new Path(Direction.Forward, Distance.MultiStep),
          new Path(Direction.Left, Distance.MultiStep),
          new Path(Direction.Right, Distance.MultiStep),
          new Path(Direction.Backward, Distance.MultiStep),
          new Path(Direction.ForwardLeft),
          new Path(Direction.ForwardRight),
          new Path(Direction.BackwardLeft),
          new Path(Direction.BackwardRight)
      });

  public static readonly ReadOnlyCollection<Path> Player2Paths =
      Player1Paths
      .Select(m => m.Invert())
      .ToList()
      .AsReadOnly();

  public static readonly ReadOnlyCollection<Path> Player2PromotedPaths =
      PromotedPlayer1Paths
      .Select(m => m.Invert())
      .ToList()
      .AsReadOnly();

  public Rook(WhichPlayer owner, bool isPromoted = false)
      : base(WhichPiece.Rook, owner, isPromoted)
  {
  }

  public override ReadOnlyCollection<Path> MoveSet => Owner switch
  {
    WhichPlayer.Player1 => IsPromoted ? PromotedPlayer1Paths : Player1Paths,
    WhichPlayer.Player2 => IsPromoted ? Player2PromotedPaths : Player2Paths,
    _ => throw new NotImplementedException(),
  };
}
