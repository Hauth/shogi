﻿using Shogi.Domain.YetToBeAssimilatedIntoDDD.Pathing;
using System.Collections.ObjectModel;

namespace Shogi.Domain.ValueObjects
{
    internal record class Knight : Piece
    {
        public static readonly ReadOnlyCollection<Path> Player1Paths = new(new List<Path>(2)
        {
            new Path(Direction.KnightLeft),
            new Path(Direction.KnightRight)
        });

        public static readonly ReadOnlyCollection<Path> Player2Paths =
            Player1Paths
            .Select(p => p.Invert())
            .ToList()
            .AsReadOnly();

        public Knight(WhichPlayer owner, bool isPromoted = false)
            : base(WhichPiece.Knight, owner, isPromoted)
        {
        }

        public override ReadOnlyCollection<Path> MoveSet => Owner switch
        {
            WhichPlayer.Player1 => IsPromoted ? GoldGeneral.Player1Paths : Player1Paths,
            WhichPlayer.Player2 => IsPromoted ? GoldGeneral.Player2Paths : Player2Paths,
            _ => throw new NotImplementedException(),
        };
    }
}
