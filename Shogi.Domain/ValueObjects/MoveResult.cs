﻿namespace Shogi.Domain.ValueObjects
{
	public record MoveResult(bool IsSuccess, string Reason = "")
	{
	}
}
