﻿using Shogi.Domain.YetToBeAssimilatedIntoDDD.Pathing;
using System.Collections.ObjectModel;

namespace Shogi.Domain.ValueObjects;

internal record class GoldGeneral : Piece
{
	public static readonly ReadOnlyCollection<Path> Player1Paths = new(
		[
				new Path(Direction.Forward),
				new Path(Direction.ForwardLeft),
				new Path(Direction.ForwardRight),
				new Path(Direction.Left),
				new Path(Direction.Right),
				new Path(Direction.Backward)
		]);

	public static readonly ReadOnlyCollection<Path> Player2Paths =
			Player1Paths
			.Select(p => p.Invert())
			.ToList()
			.AsReadOnly();

	public GoldGeneral(WhichPlayer owner, bool isPromoted = false)
			: base(WhichPiece.GoldGeneral, owner, isPromoted)
	{
	}

	public override IEnumerable<Path> MoveSet => Owner == WhichPlayer.Player1 ? Player1Paths : Player2Paths;
}
