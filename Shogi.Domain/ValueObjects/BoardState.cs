﻿using System.Collections.ObjectModel;
using Shogi.Domain.YetToBeAssimilatedIntoDDD;
using BoardTile = System.Collections.Generic.KeyValuePair<System.Numerics.Vector2, Shogi.Domain.ValueObjects.Piece>;

namespace Shogi.Domain.ValueObjects;

public class BoardState
{
	/// <summary>
	/// Board state before any moves have been made, using standard setup and rules.
	/// </summary>
	public static BoardState StandardStarting => new(
			state: BuildStandardStartingBoardState(),
			player1Hand: [],
			player2Hand: [],
			whoseTurn: WhichPlayer.Player1,
			playerInCheck: null,
			previousMove: new Move());

	/// <summary>
	/// Key is position notation, such as "E4".
	/// </summary>
	private readonly Dictionary<string, Piece?> board;

	public BoardState(
			Dictionary<string, Piece?> state,
			List<Piece> player1Hand,
			List<Piece> player2Hand,
			WhichPlayer whoseTurn,
			WhichPlayer? playerInCheck,
			Move previousMove)
	{
		board = state;
		Player1Hand = player1Hand;
		Player2Hand = player2Hand;
		PreviousMove = previousMove;
		WhoseTurn = whoseTurn;
		InCheck = playerInCheck;
	}

	/// <summary>
	/// Copy constructor. Creates a deep copy.
	/// </summary>
	public BoardState(BoardState other)
	{
		board = new(81);
		foreach (var kvp in other.board)
		{
			var piece = kvp.Value;
			board[kvp.Key] = piece == null ? null : Piece.Create(piece.WhichPiece, piece.Owner, piece.IsPromoted);
		}
		WhoseTurn = other.WhoseTurn;
		InCheck = other.InCheck;
		IsCheckmate = other.IsCheckmate;
		PreviousMove = other.PreviousMove;
		Player1Hand = new(other.Player1Hand);
		Player2Hand = new(other.Player2Hand);
	}

	public ReadOnlyDictionary<string, Piece?> State => new(board);
	public List<Piece> ActivePlayerHand => WhoseTurn == WhichPlayer.Player1 ? Player1Hand : Player2Hand;
	public Vector2 Player1KingPosition => Notation.FromBoardNotation(board.Where(kvp => kvp.Value != null).Single(kvp =>
	{
		var piece = kvp.Value;
		return piece!.IsKing() && piece!.Owner == WhichPlayer.Player1;
	}).Key);
	public Vector2 Player2KingPosition => Notation.FromBoardNotation(board.Where(kvp => kvp.Value != null).Single(kvp =>
	{
		var piece = kvp.Value;
		return piece!.IsKing() && piece!.Owner == WhichPlayer.Player2;
	}).Key);
	public List<Piece> Player1Hand { get; }
	public List<Piece> Player2Hand { get; }
	public Move PreviousMove { get; set; }
	public WhichPlayer WhoseTurn { get; set; }
	public WhichPlayer? InCheck { get; set; }
	public bool IsCheckmate { get; set; }

	public Piece? this[string notation]
	{
		// TODO: Validate "notation" here and throw an exception if invalid.
		get => board[notation];
		set => board[notation] = value;
	}

	public Piece? this[Vector2 vector]
	{
		get => this[Notation.ToBoardNotation(vector)];
		set => this[Notation.ToBoardNotation(vector)] = value;
	}

	public Piece? this[int x, int y]
	{
		get => this[Notation.ToBoardNotation(x, y)];
		set => this[Notation.ToBoardNotation(x, y)] = value;
	}

	/// <summary>
	/// Move a piece from a board tile to another board tile ignorant of check or check-mate.
	/// If a piece is captured during the move, state will change to reflect that.
	/// If a piece should promote during the move, state will change to reflect that.
	/// </summary>
	/// <param name="fromNotation">The position of the piece being moved expressed in board notation.</param>
	/// <param name="toNotation">The target position expressed in board notation.</param>
	/// <param name="isPromotionRequested">True if a promotion is expected as a result of this move.</param>
	/// <returns>A <see cref="MoveResult" /> describing the success or failure of the move.</returns>
	internal MoveResult Move(string fromNotation, string toNotation, bool isPromotionRequested = false)
	{
		var from = Notation.FromBoardNotation(fromNotation);
		var to = Notation.FromBoardNotation(toNotation);
		if (this[toNotation] != null)
		{
			Capture(to);
		}

		var fromPiece = this[fromNotation]
			?? throw new InvalidOperationException($"No piece exists at position {fromNotation}.");
		
		if (isPromotionRequested && 
			(IsWithinPromotionZone(to) || IsWithinPromotionZone(from)))
		{
			fromPiece.Promote();
		}

		this[toNotation] = fromPiece;
		this[fromNotation] = null;

		PreviousMove = new Move(from, to);
		var otherPlayer = WhoseTurn == WhichPlayer.Player1 ? WhichPlayer.Player2 : WhichPlayer.Player1;
		WhoseTurn = otherPlayer;

		return new MoveResult(true);
	}

	/// Move a piece from the hand to the board ignorant of check or check-mate.
	/// </summary>
	/// <param name="pieceInHand"></param>
	/// <param name="to">The target position expressed in board notation.</param>
	/// <returns>A <see cref="MoveResult" /> describing the success or failure of the simulation.</returns>
	internal MoveResult Move(WhichPiece pieceInHand, string toNotation)
	{
		var index = ActivePlayerHand.FindIndex(p => p.WhichPiece == pieceInHand);
		if (index == -1)
		{
			return new MoveResult(false, $"{pieceInHand} does not exist in the hand.");
		}
		if (this[toNotation] != null)
		{
			return new MoveResult(false, "Illegal move - attempting to capture while playing a piece from the hand.");
		}

		var to = Notation.FromBoardNotation(toNotation);
		switch (pieceInHand)
		{
			case WhichPiece.Knight:
				{
					// Knight cannot be placed onto the farthest two ranks from the hand.
					if (WhoseTurn == WhichPlayer.Player1 && to.Y > 6
							|| WhoseTurn == WhichPlayer.Player2 && to.Y < 2)
					{
						return new MoveResult(false, "Knight has no valid moves after placed.");
					}
					break;
				}
			case WhichPiece.Lance:
			case WhichPiece.Pawn:
				{
					// Lance and Pawn cannot be placed onto the farthest rank from the hand.
					if (WhoseTurn == WhichPlayer.Player1 && to.Y == 8
							|| WhoseTurn == WhichPlayer.Player2 && to.Y == 0)
					{
						return new MoveResult(false, $"{pieceInHand} has no valid moves after placed.");
					}
					break;
				}
		}

		// Mutate the board.
		this[toNotation] = ActivePlayerHand[index];
		ActivePlayerHand.RemoveAt(index);
		PreviousMove = new Move(pieceInHand, to);
		return new MoveResult(true);
	}

	internal bool IsWithinPromotionZone(Vector2 position)
	{
		// TODO: Move this promotion zone logic into the StandardRules class.
		return WhoseTurn == WhichPlayer.Player1 && position.Y > 5
		|| WhoseTurn == WhichPlayer.Player2 && position.Y < 3;
	}

	internal static bool IsWithinBoardBoundary(Vector2 position)
	{
		return position.X <= 8 && position.X >= 0
				&& position.Y <= 8 && position.Y >= 0;
	}

	internal List<BoardTile> GetTilesOccupiedBy(WhichPlayer whichPlayer) => board
			.Where(kvp => kvp.Value?.Owner == whichPlayer)
			.Select(kvp => new BoardTile(Notation.FromBoardNotation(kvp.Key), kvp.Value!))
			.ToList();

	internal void Capture(Vector2 to)
	{
		var piece = this[to] 
			?? throw new InvalidOperationException("Cannot capture. Piece at position does not exist.");
		piece.Capture(WhoseTurn);
		ActivePlayerHand.Add(piece);
	}

	/// <summary>
	/// Does not include the start position.
	/// </summary>
	internal static IEnumerable<Vector2> GetPathAlongDirectionFromStartToEdgeOfBoard(Vector2 start, Vector2 direction)
	{
		var next = start;
		while (IsWithinBoardBoundary(next + direction))
		{
			next += direction;
			yield return next;
		}
	}

	internal Piece? QueryFirstPieceInPath(IEnumerable<Vector2> path)
	{
		foreach (var step in path)
		{
			if (this[step] != null) return this[step];
		}
		return null;
	}

	private static Dictionary<string, Piece?> BuildStandardStartingBoardState()
	{
		return new Dictionary<string, Piece?>(81)
		{
			["A1"] = new Lance(WhichPlayer.Player1),
			["B1"] = new Knight(WhichPlayer.Player1),
			["C1"] = new SilverGeneral(WhichPlayer.Player1),
			["D1"] = new GoldGeneral(WhichPlayer.Player1),
			["E1"] = new King(WhichPlayer.Player1),
			["F1"] = new GoldGeneral(WhichPlayer.Player1),
			["G1"] = new SilverGeneral(WhichPlayer.Player1),
			["H1"] = new Knight(WhichPlayer.Player1),
			["I1"] = new Lance(WhichPlayer.Player1),

			["A2"] = null,
			["B2"] = new Bishop(WhichPlayer.Player1),
			["C2"] = null,
			["D2"] = null,
			["E2"] = null,
			["F2"] = null,
			["G2"] = null,
			["H2"] = new Rook(WhichPlayer.Player1),
			["I2"] = null,

			["A3"] = new Pawn(WhichPlayer.Player1),
			["B3"] = new Pawn(WhichPlayer.Player1),
			["C3"] = new Pawn(WhichPlayer.Player1),
			["D3"] = new Pawn(WhichPlayer.Player1),
			["E3"] = new Pawn(WhichPlayer.Player1),
			["F3"] = new Pawn(WhichPlayer.Player1),
			["G3"] = new Pawn(WhichPlayer.Player1),
			["H3"] = new Pawn(WhichPlayer.Player1),
			["I3"] = new Pawn(WhichPlayer.Player1),

			["A4"] = null,
			["B4"] = null,
			["C4"] = null,
			["D4"] = null,
			["E4"] = null,
			["F4"] = null,
			["G4"] = null,
			["H4"] = null,
			["I4"] = null,

			["A5"] = null,
			["B5"] = null,
			["C5"] = null,
			["D5"] = null,
			["E5"] = null,
			["F5"] = null,
			["G5"] = null,
			["H5"] = null,
			["I5"] = null,

			["A6"] = null,
			["B6"] = null,
			["C6"] = null,
			["D6"] = null,
			["E6"] = null,
			["F6"] = null,
			["G6"] = null,
			["H6"] = null,
			["I6"] = null,

			["A7"] = new Pawn(WhichPlayer.Player2),
			["B7"] = new Pawn(WhichPlayer.Player2),
			["C7"] = new Pawn(WhichPlayer.Player2),
			["D7"] = new Pawn(WhichPlayer.Player2),
			["E7"] = new Pawn(WhichPlayer.Player2),
			["F7"] = new Pawn(WhichPlayer.Player2),
			["G7"] = new Pawn(WhichPlayer.Player2),
			["H7"] = new Pawn(WhichPlayer.Player2),
			["I7"] = new Pawn(WhichPlayer.Player2),

			["A8"] = null,
			["B8"] = new Rook(WhichPlayer.Player2),
			["C8"] = null,
			["D8"] = null,
			["E8"] = null,
			["F8"] = null,
			["G8"] = null,
			["H8"] = new Bishop(WhichPlayer.Player2),
			["I8"] = null,

			["A9"] = new Lance(WhichPlayer.Player2),
			["B9"] = new Knight(WhichPlayer.Player2),
			["C9"] = new SilverGeneral(WhichPlayer.Player2),
			["D9"] = new GoldGeneral(WhichPlayer.Player2),
			["E9"] = new King(WhichPlayer.Player2),
			["F9"] = new GoldGeneral(WhichPlayer.Player2),
			["G9"] = new SilverGeneral(WhichPlayer.Player2),
			["H9"] = new Knight(WhichPlayer.Player2),
			["I9"] = new Lance(WhichPlayer.Player2)
		};
	}
}
