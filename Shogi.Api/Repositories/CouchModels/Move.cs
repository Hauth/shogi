﻿using Shogi.Domain;

namespace Shogi.Api.Repositories.CouchModels
{
  public class Move
  {
    /// <summary>
    /// A board coordinate, like A3 or G6. When null, look for PieceFromHand to exist.
    /// </summary>
    public string? From { get; set; }

    public bool IsPromotion { get; set; }

    /// <summary>
    /// The piece placed from the player's hand.
    /// </summary>
    public WhichPiece? PieceFromHand { get; set; }

    /// <summary>
    /// A board coordinate, like A3 or G6.
    /// </summary>
    public string To { get; set; }

    /// <summary>
    /// Default constructor and setters are for deserialization.
    /// </summary>
    public Move()
    {
      To = string.Empty;
    }
  }
}
