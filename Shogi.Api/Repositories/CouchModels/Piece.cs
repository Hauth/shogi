﻿using Shogi.Domain;

namespace Shogi.Api.Repositories.CouchModels
{
	public class Piece
	{
		public bool IsPromoted { get; set; }
		public WhichPlayer Owner { get; set; }
		public WhichPiece WhichPiece { get; set; }
	}
}
