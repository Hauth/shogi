﻿using Shogi.Domain;

namespace Shogi.Api.Repositories.CouchModels
{
    public class BoardStateDocument : CouchDocument
	{
		public string Name { get; set; }

		/// <summary>
		/// A dictionary where the key is a board-notation position, like D3.
		/// </summary>
		public Dictionary<string, Piece?> Board { get; set; }

		public Piece[] Player1Hand { get; set; }

		public Piece[] Player2Hand { get; set; }

		/// <summary>
		/// Move is null for first BoardState of a session - before anybody has made moves.
		/// </summary>
		public Move? Move { get; set; }

		/// <summary>
		/// Default constructor and setters are for deserialization.
		/// </summary>
		public BoardStateDocument() : base(WhichDocumentType.BoardState)
		{
			Name = string.Empty;
			Board = new Dictionary<string, Piece?>(81, StringComparer.OrdinalIgnoreCase);
			Player1Hand = Array.Empty<Piece>();
			Player2Hand = Array.Empty<Piece>();
		}

		public BoardStateDocument(string sessionName, Session shogi)
			: base($"{sessionName}-{DateTime.Now:O}", WhichDocumentType.BoardState)
		{
			static Piece MapPiece(Domain.ValueObjects.Piece piece)
			{
				return new Piece { IsPromoted = piece.IsPromoted, Owner = piece.Owner, WhichPiece = piece.WhichPiece };
			}

			Name = sessionName;
			Board = shogi.BoardState.State.ToDictionary(kvp => kvp.Key, kvp => kvp.Value == null ? null : MapPiece(kvp.Value));

			Player1Hand = shogi.BoardState.Player1Hand.Select(piece => MapPiece(piece)).ToArray();
			Player2Hand = shogi.BoardState.Player2Hand.Select(piece => MapPiece(piece)).ToArray();
		}
	}
}
