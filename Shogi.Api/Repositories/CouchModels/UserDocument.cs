﻿using Shogi.Api.Models;

namespace Shogi.Api.Repositories.CouchModels
{
  public class UserDocument : CouchDocument
  {
    public string DisplayName { get; set; }
    public WhichLoginPlatform Platform { get; set; }

    /// <summary>
    /// Constructor for JSON deserializing.
    /// </summary>
    public UserDocument() : base(WhichDocumentType.User)
    {
      DisplayName = string.Empty;
    }

    public UserDocument(
      string id,
      string displayName,
      WhichLoginPlatform platform) : base(id, WhichDocumentType.User)
    {
      DisplayName = displayName;
      Platform = platform;
    }
  }
}
