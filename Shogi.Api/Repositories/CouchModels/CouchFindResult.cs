﻿using System;

namespace Shogi.Api.Repositories.CouchModels
{
  internal class CouchFindResult<T>
  {
    public T[] docs;
    public string warning;

    public CouchFindResult()
    {
      docs = Array.Empty<T>();
      warning = "";
    }
  }
}
