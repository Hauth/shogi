﻿using System;

namespace Shogi.Api.Repositories.CouchModels
{
  public class CouchViewResult<T> where T : class
  {
    public int total_rows;
    public int offset;
    public CouchViewResultRow<T>[] rows;

    public CouchViewResult()
    {
      rows = Array.Empty<CouchViewResultRow<T>>();
    }
  }

  public class CouchViewResultRow<T>
  {
    public string id;
    public T doc;

    public CouchViewResultRow()
    {
      id = string.Empty;
      doc = default!;
    }
  }
}
