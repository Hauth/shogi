﻿namespace Shogi.Api.Repositories.CouchModels
{
  public class CouchCreateResult
  {
    public string Id { get; set; }
    public bool Ok { get; set; }
    public string Rev { get; set; }

    public CouchCreateResult()
    {
      Id = string.Empty;
      Rev = string.Empty;
    }
  }
}
