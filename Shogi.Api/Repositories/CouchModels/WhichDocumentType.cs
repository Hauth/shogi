﻿namespace Shogi.Api.Repositories.CouchModels
{
  public enum WhichDocumentType
  {
    User,
    Session,
    BoardState
  }
}
