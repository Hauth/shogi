﻿using Shogi.Contracts.Types;

namespace Shogi.Api.Repositories.CouchModels
{
  public class SessionDocument : CouchDocument
  {
    public string Name { get; set; }
    public string Player1Id { get; set; }
    public string? Player2Id { get; set; }
    public bool IsPrivate { get; set; }

    /// <summary>
    /// Default constructor and setters are for deserialization.
    /// </summary>
    public SessionDocument() : base(WhichDocumentType.Session)
    {
      Name = string.Empty;
      Player1Id = string.Empty;
      Player2Id = string.Empty;
    }

    public SessionDocument(SessionMetadata sessionMetaData)
      : base(sessionMetaData.Name, WhichDocumentType.Session)
    {
      Name = sessionMetaData.Name;
    }
  }
}
