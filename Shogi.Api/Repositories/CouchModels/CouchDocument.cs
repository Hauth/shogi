﻿using Newtonsoft.Json;
using System;

namespace Shogi.Api.Repositories.CouchModels
{
	[Obsolete]
	public abstract class CouchDocument
	{
		[JsonProperty("_id")] public string Id { get; set; }
		[JsonProperty("_rev")] public string? RevisionId { get; set; }
		public WhichDocumentType DocumentType { get; }
		public DateTimeOffset CreatedDate { get; set; }

		public CouchDocument(WhichDocumentType documentType)
			: this(string.Empty, documentType, DateTimeOffset.UtcNow) { }

		public CouchDocument(string id, WhichDocumentType documentType)
			: this(id, documentType, DateTimeOffset.UtcNow) { }

		public CouchDocument(string id, WhichDocumentType documentType, DateTimeOffset createdDate)
		{
			Id = id;
			DocumentType = documentType;
			CreatedDate = createdDate;
		}
	}
}
