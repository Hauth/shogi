﻿using Dapper;
using Shogi.Api.Repositories.Dto;
using System.Data;
using System.Data.SqlClient;

namespace Shogi.Api.Repositories;

public class QueryRepository(IConfiguration configuration)
{
	private readonly string connectionString = configuration.GetConnectionString("ShogiDatabase")
		?? throw new InvalidOperationException("No database configured for QueryRepository.");

	public async Task<IEnumerable<SessionDto>> ReadSessionsMetadata(string playerId)
	{
		using var connection = new SqlConnection(this.connectionString);

		var results = await connection.QueryMultipleAsync(
				"session.ReadSessionsMetadata",
				new { PlayerId = playerId },
				commandType: CommandType.StoredProcedure);

		return await results.ReadAsync<SessionDto>();
	}
}
