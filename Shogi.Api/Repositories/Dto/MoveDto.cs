﻿using Shogi.Domain.ValueObjects;

namespace Shogi.Api.Repositories.Dto;

/// <summary>
/// Useful with Dapper to read from database.
/// </summary>
public readonly record struct MoveDto(string? From, string To, bool? IsPromotion, WhichPiece? PieceFromHand)
{
}
