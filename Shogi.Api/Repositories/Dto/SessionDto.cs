﻿namespace Shogi.Api.Repositories.Dto;

public readonly record struct SessionDto(string Id, string Player1Id, string Player2Id)
{
}
