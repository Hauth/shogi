﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using System.Net.Http.Headers;
using System.Text.Json;

namespace Shogi.Api.Repositories;

// https://app-smtp.brevo.com/real-time

public class EmailSender : IEmailSender
{
	private static readonly JsonSerializerOptions Options = new(JsonSerializerDefaults.Web);
	private readonly HttpClient client;
	private string apiKey;

	public EmailSender(HttpClient client, IOptionsMonitor<ApiKeys> apiKeys)
	{
		this.apiKey = apiKeys.CurrentValue.BrevoEmailService;
		apiKeys.OnChange(keys => this.apiKey = keys.BrevoEmailService);
		this.client = client;
	}

	public async Task SendEmailAsync(string email, string subject, string htmlMessage)
	{
		var body = new
		{
			Sender = new
			{
				Name = "Shogi Account Support",
				Email = "shogi@lucaserver.space",
			},
			To = new[]
			{
				new
				{
					Name = email,
					Email = email,
				}
			},
			Subject = subject,
			HtmlContent = htmlMessage,
		};

		var request = new HttpRequestMessage(HttpMethod.Post, new Uri("https://api.brevo.com/v3/smtp/email", UriKind.Absolute));
		request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
		request.Headers.Add("api-key", apiKey);
		request.Content = JsonContent.Create(body, options: Options);

		var response = await this.client.SendAsync(request);
		response.EnsureSuccessStatusCode();
	}
}
