﻿using System.Security.Claims;

namespace Shogi.Api.Controllers;

public static class Extentions
{
	public static string? GetId(this ClaimsPrincipal self)
	{
		return self.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value;
	}
}
