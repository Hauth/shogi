﻿using Shogi.Contracts.Types;
using System.Collections.ObjectModel;

namespace Shogi.Api.Extensions;

public static class ContractsExtensions
{
	public static WhichPlayer ToContract(this Domain.ValueObjects.WhichPlayer player)
	{
		return player switch
		{
			Domain.ValueObjects.WhichPlayer.Player1 => WhichPlayer.Player1,
			Domain.ValueObjects.WhichPlayer.Player2 => WhichPlayer.Player2,
			_ => throw new NotImplementedException(),
		};
	}

	public static WhichPiece ToContract(this Domain.ValueObjects.WhichPiece piece)
	{
		return piece switch
		{
            Domain.ValueObjects.WhichPiece.King => WhichPiece.King,
            Domain.ValueObjects.WhichPiece.GoldGeneral => WhichPiece.GoldGeneral,
            Domain.ValueObjects.WhichPiece.SilverGeneral => WhichPiece.SilverGeneral,
            Domain.ValueObjects.WhichPiece.Bishop => WhichPiece.Bishop,
            Domain.ValueObjects.WhichPiece.Rook => WhichPiece.Rook,
            Domain.ValueObjects.WhichPiece.Knight => WhichPiece.Knight,
            Domain.ValueObjects.WhichPiece.Lance => WhichPiece.Lance,
            Domain.ValueObjects.WhichPiece.Pawn => WhichPiece.Pawn,
			_ => throw new NotImplementedException(),
		};
	}

	public static Piece ToContract(this Domain.ValueObjects.Piece piece) => new()
	{
		IsPromoted = piece.IsPromoted,
		Owner = piece.Owner.ToContract(),
		WhichPiece = piece.WhichPiece.ToContract()
	};

	public static IReadOnlyList<Piece> ToContract(this List<Domain.ValueObjects.Piece> pieces)
	{
		return pieces
			.Select(ToContract)
			.ToList()
			.AsReadOnly();
	}

	public static Dictionary<string, Piece?> ToContract(this ReadOnlyDictionary<string, Domain.ValueObjects.Piece?> boardState) =>
		boardState.ToDictionary(kvp => kvp.Key, kvp => kvp.Value?.ToContract());

	public static Domain.ValueObjects.WhichPiece ToDomain(this WhichPiece piece)
	{
		return piece switch
		{
			WhichPiece.King => Domain.ValueObjects.WhichPiece.King,
			WhichPiece.GoldGeneral => Domain.ValueObjects.WhichPiece.GoldGeneral,
			WhichPiece.SilverGeneral => Domain.ValueObjects.WhichPiece.SilverGeneral,
			WhichPiece.Bishop => Domain.ValueObjects.WhichPiece.Bishop,
			WhichPiece.Rook => Domain.ValueObjects.WhichPiece.Rook,
			WhichPiece.Knight => Domain.ValueObjects.WhichPiece.Knight,
			WhichPiece.Lance => Domain.ValueObjects.WhichPiece.Lance,
			WhichPiece.Pawn => Domain.ValueObjects.WhichPiece.Pawn,
			_ => throw new NotImplementedException(),
		};
	}
}
