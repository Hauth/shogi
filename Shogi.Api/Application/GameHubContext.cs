﻿using Microsoft.AspNetCore.SignalR;

namespace Shogi.Api.Application;

/// <summary>
/// Used to send signals to connected clients.
/// </summary>
public class GameHubContext(IHubContext<GameHub> context)
{
	public async Task Emit_SessionJoined(string sessionId)
	{
		var clients = context.Clients.Group(sessionId);
		await clients.SendAsync("SessionJoined");
	}

	public async Task Emit_PieceMoved(string sessionId)
	{
		var clients = context.Clients.Group(sessionId);
		await clients.SendAsync("PieceMoved");
	}
}
