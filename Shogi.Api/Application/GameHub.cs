﻿using Microsoft.AspNetCore.SignalR;

namespace Shogi.Api.Application;

/// <summary>
/// Used to receive signals from connected clients.
/// </summary>
public class GameHub : Hub
{
	public Task Subscribe(string sessionId)
	{
		return this.Groups.AddToGroupAsync(this.Context.ConnectionId, sessionId);
	}

	public Task Unsubscribe(string sessionId)
	{
		return this.Groups.RemoveFromGroupAsync(this.Context.ConnectionId, sessionId);
	}
}
