﻿using System;

namespace Shogi.Contracts.Types;

public class Session
{
	/// <summary>
	/// Email
	/// </summary>
	public string Player1 { get; set; } = string.Empty;

	/// <summary>
	/// Email. Null if no second player exists.
	/// </summary>
	public string? Player2 { get; set; }

	public Guid SessionId { get; set; }

	public BoardState BoardState { get; set; }
}
