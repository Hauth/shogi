﻿using System;
using System.Collections.Generic;

namespace Shogi.Contracts.Types;

public class BoardState
{
	public Dictionary<string, Piece?> Board { get; set; } = new Dictionary<string, Piece?>();
	public IReadOnlyCollection<Piece> Player1Hand { get; set; } = Array.Empty<Piece>();
	public IReadOnlyCollection<Piece> Player2Hand { get; set; } = Array.Empty<Piece>();
	public WhichPlayer? PlayerInCheck { get; set; }
	public WhichPlayer WhoseTurn { get; set; }
	public WhichPlayer? Victor { get; set; }
}
