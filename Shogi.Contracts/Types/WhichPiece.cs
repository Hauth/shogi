﻿namespace Shogi.Contracts.Types;

public enum WhichPiece
{
	King,
	GoldGeneral,
	SilverGeneral,
	Bishop,
	Rook,
	Knight,
	Lance,
	Pawn
}
