﻿namespace Shogi.Contracts.Types
{
  public class Piece
  {
    public bool IsPromoted { get; set; }
    public WhichPiece WhichPiece { get; set; }
    public WhichPlayer Owner { get; set; }
  }
}
