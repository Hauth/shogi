﻿using System;

namespace Shogi.Contracts.Types;

public class SessionMetadata
{
	public Guid SessionId { get; set; }
	public string Player1 { get; set; } = string.Empty;
	public string Player2 { get; set; } = string.Empty;
}
