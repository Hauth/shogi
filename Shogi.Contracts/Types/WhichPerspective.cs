﻿namespace Shogi.Contracts.Types;

public enum WhichPlayer
{
	Player1,
	Player2
}
