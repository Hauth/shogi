﻿using System.Text.Json;

namespace Shogi.Contracts;

public class ShogiApiJsonSerializerSettings
{
	public readonly static JsonSerializerOptions SystemTextJsonSerializerOptions = new(JsonSerializerDefaults.Web)
	{
		WriteIndented = true,
	};
}
