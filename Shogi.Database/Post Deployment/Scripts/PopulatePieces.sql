﻿DECLARE @Pieces TABLE(
	[Name] NVARCHAR(13)
)

INSERT INTO @Pieces ([Name])
VALUES
	('King'),
	('GoldGeneral'),
	('SilverGeneral'),
	('Bishop'),
	('Rook'),
	('Knight'),
	('Lance'),
	('Pawn');

MERGE [session].[Piece] as t
USING @Pieces as s
ON t.[Name] = s.[Name]
WHEN NOT MATCHED THEN
	INSERT ([Name])
	VALUES (s.[Name]);