﻿CREATE TABLE [session].[Move]
(
	[Id]							INT NOT NULL PRIMARY KEY IDENTITY,
	[SessionId]				[session].[SessionSurrogateKey]	NOT NULL,
	[To]							VARCHAR(2) NOT NULL,
	[From]						VARCHAR(2) NULL,
	[PieceIdFromHand]	INT NULL,
	[IsPromotion]			BIT DEFAULT 0

	CONSTRAINT [Cannot end where you start]
		CHECK ([From] <> [To]),

	CONSTRAINT [Move cannot start from two places]
		CHECK (
			( [From] IS NOT NULL AND [PieceIdFromHand] IS NULL )
			OR
			( [From] IS NULL AND [PieceIdFromhand] IS NOT NULL )
		),

	CONSTRAINT FK_Move_Session FOREIGN KEY (SessionId) REFERENCES [session].[Session] (Id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,

	CONSTRAINT FK_Move_Piece FOREIGN KEY (PieceIdFromHand) REFERENCES [session].[Piece] (Id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
