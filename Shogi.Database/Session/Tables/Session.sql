﻿CREATE TABLE [session].[Session]
(
	Id					[session].[SessionSurrogateKey] PRIMARY KEY,
	Player1Id		[dbo].[AspNetUsersId] NOT NULL,
	Player2Id		[dbo].[AspNetUsersId] NULL,
	[CreatedDate]			DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(), 
  CONSTRAINT [CK_Session_LimitedNewSessions] CHECK ([session].MaxNewSessionsPerUser() < 4),
)
