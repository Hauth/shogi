﻿CREATE FUNCTION [session].[MaxNewSessionsPerUser]() RETURNS INT
AS
BEGIN
	
	DECLARE @MaxNewSessionsCreatedByAnyOneUser INT;

	WITH CountOfNewSessionsPerPlayer AS
	(
	SELECT COUNT(*) as TotalNewSessions
		FROM [session].[Session]
		WHERE Player2Id IS NULL
		GROUP BY Player1Id
	)
	SELECT @MaxNewSessionsCreatedByAnyOneUser = MAX(CountOfNewSessionsPerPlayer.TotalNewSessions)
	FROM CountOfNewSessionsPerPlayer

	RETURN @MaxNewSessionsCreatedByAnyOneUser
END
