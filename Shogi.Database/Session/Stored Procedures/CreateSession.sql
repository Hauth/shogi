﻿CREATE PROCEDURE [session].[CreateSession]
	@Id [session].[SessionSurrogateKey],
	@Player1Id [dbo].[AspNetUsersId]
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO [session].[Session] 
	([Id], Player1Id)
	VALUES
	(@Id, @Player1Id)
	
END