﻿CREATE PROCEDURE [session].[CreateMove]
	@To							VARCHAR(2),
	@From						VARCHAR(2) = NULL,
	@IsPromotion		BIT = 0,
	@PieceFromHand	NVARCHAR(13) = NULL,
	@SessionId			[session].[SessionSurrogateKey]
AS

BEGIN
	SET NOCOUNT ON -- Performance boost
	SET XACT_ABORT ON -- Rollback transaction on error
	SET TRANSACTION ISOLATION LEVEL SNAPSHOT -- Ignores data changes that happen after the transaction begins.

	BEGIN TRANSACTION

		DECLARE @PieceIdFromhand INT = NULL;
		SELECT @PieceIdFromhand = Id
		FROM [session].[Piece]
		WHERE [Name] = @PieceFromHand;

		INSERT INTO [session].[Move] 
		(SessionId, [To], [From], IsPromotion, PieceIdFromHand)
		VALUES
		(@SessionId, @To, @From, @IsPromotion, @PieceIdFromhand);

	COMMIT
END