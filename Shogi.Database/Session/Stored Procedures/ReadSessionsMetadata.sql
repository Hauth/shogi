﻿CREATE PROCEDURE [session].[ReadSessionsMetadata]
	@PlayerId [dbo].[AspNetUsersId]
AS
BEGIN
	SET NOCOUNT ON;

	-- Read all sessions, in this order:
	--	1. sessions created by the logged-in user
	--	2. any other sessions the logged-in user participates in
	--	3. all other sessions
	SELECT
		Id, Player1Id, Player2Id, [Session].CreatedDate,
		case 
			when Player1Id = @PlayerId then 0 
			when Player2Id = @PlayerId then 1
			else 2
		end as OrderBy
	FROM [session].[Session]
	Order By OrderBy ASC, CreatedDate DESC

END