﻿CREATE PROCEDURE [session].[ReadSession]
	@Id [session].[SessionSurrogateKey]
AS
BEGIN
	SET NOCOUNT ON -- Performance boost
	SET XACT_ABORT ON -- Rollback transaction on error
	SET TRANSACTION ISOLATION LEVEL SNAPSHOT -- Ignores data changes that happen after the transaction begins.

	BEGIN TRANSACTION

		-- Session
		SELECT
			Id,
			Player1Id,
			Player2Id,
			CreatedDate
		FROM [session].[Session]
		WHERE Id = @Id;

		-- Player moves
		SELECT
			mv.[From],
			mv.[To],
			mv.IsPromotion,
			piece.[Name] as PieceFromHand
		FROM [session].[Move] mv
			INNER JOIN [session].[Session] sess ON sess.Id = mv.SessionId
			LEFT JOIN [session].Piece piece on piece.Id = mv.PieceIdFromHand
		WHERE sess.[Id] = @Id;
		
	COMMIT
END
