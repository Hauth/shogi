﻿CREATE PROCEDURE [session].[DeleteSession]
	@Id [session].[SessionSurrogateKey]
AS

DELETE FROM [session].[Session] WHERE [Id] = @Id;