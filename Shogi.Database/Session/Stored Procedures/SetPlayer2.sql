﻿CREATE PROCEDURE [session].[SetPlayer2]
	@SessionId [session].[SessionSurrogateKey],
	@PlayerId [dbo].[AspNetUsersId]
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE [session].[Session]
	SET Player2Id = @PlayerId
	FROM [session].[Session]
	WHERE Id = @SessionId;

END
