﻿CREATE TYPE [session].[SessionSurrogateKey]
	FROM CHAR(36) NOT NULL
