﻿-- This is so I don't have to remember the type used in the dbo.AspNetUsers table for the Id column.
CREATE TYPE [dbo].[AspNetUsersId]
	FROM NVARCHAR(450) NOT NULL;
