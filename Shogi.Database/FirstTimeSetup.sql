﻿-- Create a user named Shogi.Api

-- Create a role and grant execute permission to that role
--CREATE ROLE db_executor
--GRANT EXECUTE To db_executor

-- Give Shogi.Api user permission to db_executor, db_datareader, db_datawriter


/**
* Local setup instructions, in order:
*		1. To setup the Shogi database, use the publish menu option in visual studio with the Shogi.Database project.
* 
*		2. Setup Entity Framework because that's what the login system uses.
*		2.a. Install the Entity Framework dotnet tools, via power shell run this command: dotnet tool install --global dotnet-ef
*		2.b. To setup the Entity Framework users database, run this powershell command using Shogi.Api as the target project: dotnet ef database update
*/