﻿using Shogi.Domain.ValueObjects;
using Shogi.Domain.YetToBeAssimilatedIntoDDD.Pathing;
using System.Numerics;

namespace UnitTests;

public class RookShould
{
	public class MoveSet
	{
		private readonly Rook rook1;
		private readonly Rook rook2;

		public MoveSet()
		{
			rook1 = new Rook(WhichPlayer.Player1);
			rook2 = new Rook(WhichPlayer.Player2);
		}

		[Fact]
		public void Player1_HasCorrectMoveSet()
		{
			var moveSet = rook1.MoveSet;
			moveSet.Should().HaveCount(4);
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Forward, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Left, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Right, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Backward, Distance.MultiStep));
		}

		[Fact]
		public void Player1_Promoted_HasCorrectMoveSet()
		{
			// Arrange
			rook1.Promote();
			rook1.IsPromoted.Should().BeTrue();

			// Assert
			var moveSet = rook1.MoveSet;
			moveSet.Should().HaveCount(8);
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Forward, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Left, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Right, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Backward, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.ForwardLeft, Distance.OneStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.BackwardLeft, Distance.OneStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.ForwardRight, Distance.OneStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.BackwardRight, Distance.OneStep));
		}

		[Fact]
		public void Player2_HasCorrectMoveSet()
		{
			var moveSet = rook2.MoveSet;
			moveSet.Should().HaveCount(4);
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Forward, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Left, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Right, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Backward, Distance.MultiStep));
		}

		[Fact]
		public void Player2_Promoted_HasCorrectMoveSet()
		{
			// Arrange
			rook2.Promote();
			rook2.IsPromoted.Should().BeTrue();

			// Assert
			var moveSet = rook2.MoveSet;
			moveSet.Should().HaveCount(8);
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Forward, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Left, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Right, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.Backward, Distance.MultiStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.ForwardLeft, Distance.OneStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.BackwardLeft, Distance.OneStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.ForwardRight, Distance.OneStep));
			moveSet.Should().ContainEquivalentOf(new Path(Direction.BackwardRight, Distance.OneStep));
		}


	}
	private readonly Rook rookPlayer1;

	public RookShould()
	{
		rookPlayer1 = new Rook(WhichPlayer.Player1);
	}

	[Fact]
	public void Promote()
	{
		rookPlayer1.IsPromoted.Should().BeFalse();
		rookPlayer1.CanPromote.Should().BeTrue();
		rookPlayer1.Promote();
		rookPlayer1.IsPromoted.Should().BeTrue();
		rookPlayer1.CanPromote.Should().BeFalse();
	}

	[Fact]
	public void GetStepsFromStartToEnd_Player1NotPromoted_LateralMove()
	{
		Vector2 start = new(0, 0);
		Vector2 end = new(0, 5);

		var steps = rookPlayer1.GetPathFromStartToEnd(start, end);

		rookPlayer1.IsPromoted.Should().BeFalse();
		steps.Should().HaveCount(5);
		steps.Should().Contain(new Vector2(0, 1));
		steps.Should().Contain(new Vector2(0, 2));
		steps.Should().Contain(new Vector2(0, 3));
		steps.Should().Contain(new Vector2(0, 4));
		steps.Should().Contain(new Vector2(0, 5));
	}

	[Fact]
	public void GetStepsFromStartToEnd_Player1NotPromoted_DiagonalMove()
	{
		Vector2 start = new(0, 0);
		Vector2 end = new(1, 1);

		var steps = rookPlayer1.GetPathFromStartToEnd(start, end);

		rookPlayer1.IsPromoted.Should().BeFalse();
		steps.Should().BeEmpty();
	}

	[Fact]
	public void GetStepsFromStartToEnd_Player1Promoted_LateralMove()
	{
		Vector2 start = new(0, 0);
		Vector2 end = new(0, 5);
		rookPlayer1.Promote();

		var steps = rookPlayer1.GetPathFromStartToEnd(start, end);

		rookPlayer1.IsPromoted.Should().BeTrue();
		steps.Should().HaveCount(5);
		steps.Should().Contain(new Vector2(0, 1));
		steps.Should().Contain(new Vector2(0, 2));
		steps.Should().Contain(new Vector2(0, 3));
		steps.Should().Contain(new Vector2(0, 4));
		steps.Should().Contain(new Vector2(0, 5));
	}

	[Fact]
	public void GetStepsFromStartToEnd_Player1Promoted_DiagonalMove()
	{
		Vector2 start = new(0, 0);
		Vector2 end = new(1, 1);
		rookPlayer1.Promote();

		var steps = rookPlayer1.GetPathFromStartToEnd(start, end);

		rookPlayer1.IsPromoted.Should().BeTrue();
		steps.Should().HaveCount(1);
		steps.Should().Contain(new Vector2(1, 1));
	}

	[Fact]
	public void GetStepsFromStartToEnd_Player2NotPromoted_LateralMove()
	{
		Vector2 start = new(0, 0);
		Vector2 end = new(0, 5);

		var steps = rookPlayer1.GetPathFromStartToEnd(start, end);

		rookPlayer1.IsPromoted.Should().BeFalse();
		steps.Should().HaveCount(5);
		steps.Should().Contain(new Vector2(0, 1));
		steps.Should().Contain(new Vector2(0, 2));
		steps.Should().Contain(new Vector2(0, 3));
		steps.Should().Contain(new Vector2(0, 4));
		steps.Should().Contain(new Vector2(0, 5));
	}

	[Fact]
	public void GetStepsFromStartToEnd_Player2NotPromoted_DiagonalMove()
	{
		Vector2 start = new(0, 0);
		Vector2 end = new(1, 1);

		var steps = rookPlayer1.GetPathFromStartToEnd(start, end);

		rookPlayer1.IsPromoted.Should().BeFalse();
		steps.Should().BeEmpty();
	}

	[Fact]
	public void GetStepsFromStartToEnd_Player2Promoted_LateralMove()
	{
		Vector2 start = new(0, 0);
		Vector2 end = new(0, 5);
		rookPlayer1.Promote();

		var steps = rookPlayer1.GetPathFromStartToEnd(start, end);

		rookPlayer1.IsPromoted.Should().BeTrue();
		steps.Should().HaveCount(5);
		steps.Should().Contain(new Vector2(0, 1));
		steps.Should().Contain(new Vector2(0, 2));
		steps.Should().Contain(new Vector2(0, 3));
		steps.Should().Contain(new Vector2(0, 4));
		steps.Should().Contain(new Vector2(0, 5));
	}

	[Fact]
	public void GetStepsFromStartToEnd_Player2Promoted_DiagonalMove()
	{
		Vector2 start = new(0, 0);
		Vector2 end = new(1, 1);
		rookPlayer1.Promote();

		var steps = rookPlayer1.GetPathFromStartToEnd(start, end);

		rookPlayer1.IsPromoted.Should().BeTrue();
		steps.Should().HaveCount(1);
		steps.Should().Contain(new Vector2(1, 1));
	}
}
