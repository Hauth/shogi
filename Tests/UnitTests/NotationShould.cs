﻿using System.Numerics;
using Shogi.Domain.YetToBeAssimilatedIntoDDD;

namespace UnitTests
{
	public class NotationShould
	{
		[Fact]
		public void ConvertFromNotationToVector()
		{
			Notation.FromBoardNotation("A1").Should().Be(new Vector2(0, 0));
			Notation.FromBoardNotation("E5").Should().Be(new Vector2(4, 4));
			Notation.FromBoardNotation("I9").Should().Be(new Vector2(8, 8));
			Notation.FromBoardNotation("C3").Should().Be(new Vector2(2, 2));
		}

		[Fact]
		public void ConvertFromVectorToNotation()
		{
			Notation.ToBoardNotation(new Vector2(0, 0)).Should().Be("A1");
			Notation.ToBoardNotation(new Vector2(4, 4)).Should().Be("E5");
			Notation.ToBoardNotation(new Vector2(8, 8)).Should().Be("I9");
			Notation.ToBoardNotation(new Vector2(2, 2)).Should().Be("C3");
		}
	}
}
