﻿using Shogi.Domain.ValueObjects;
using System;
using System.Text;

namespace UnitTests;

public static class Extensions
{
	/// <summary>
	/// Prints a ASCII representation of the board for debugging board state.
	/// </summary>
	/// <returns></returns>
	public static string ToStringStateAsAscii(this ShogiBoard board)
	{
		var boardState = board.BoardState;
		var builder = new StringBuilder();
		builder.Append("                    ");
		builder.Append("Player 2");
		builder.AppendLine();
		for (var rank = 8; rank >= 0; rank--)
		{
			// Horizontal line
			builder.Append("     - ");
			for (var file = 0; file < 8; file++) builder.Append("- - ");
			builder.Append("- -");

			// Print Rank ruler.
			builder.AppendLine();
			builder.Append($"{rank + 1}  ");

			// Print pieces.
			builder.Append("  |");
			for (var x = 0; x < 9; x++)
			{
				var piece = boardState[x, rank];
				if (piece == null)
				{
					builder.Append("   ");
				}
				else
				{
					builder.AppendFormat("{0}", ToAscii(piece));
				}
				builder.Append('|');
			}
			builder.AppendLine();
		}

		// Horizontal line
		builder.Append("     - ");
		for (var x = 0; x < 8; x++) builder.Append("- - ");
		builder.Append("- -");
		builder.AppendLine();
		// Print File ruler.
		builder.Append("     ");
		builder.Append("  A   B   C   D   E   F   G   H   I  ");

		builder.AppendLine();
		builder.AppendLine();
		builder.Append("                    ");
		builder.Append("Player 1");

		return builder.ToString();
	}

	/// <returns>
	/// A string with three characters. 
	/// The first character indicates promotion status. 
	/// The second character indicates piece. 
	/// The third character indicates ownership.
	/// </returns>
	private static string ToAscii(Piece piece)
	{
		var builder = new StringBuilder();
		if (piece.IsPromoted) builder.Append('^');
		else builder.Append(' ');

		var name = piece.WhichPiece switch
		{
			WhichPiece.King => "K",
			WhichPiece.GoldGeneral => "G",
			WhichPiece.SilverGeneral => "S",
			WhichPiece.Bishop => "B",
			WhichPiece.Rook => "R",
			WhichPiece.Knight => "k",
			WhichPiece.Lance => "L",
			WhichPiece.Pawn => "P",
			_ => throw new ArgumentException($"Unknown value for {nameof(WhichPiece)}."),
		};
		builder.Append(name);

		if (piece.Owner == WhichPlayer.Player2) builder.Append('.');
		else builder.Append(' ');

		return builder.ToString();
	}
}
