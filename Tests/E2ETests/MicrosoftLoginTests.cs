﻿using Microsoft.Playwright;
using Microsoft.Playwright.NUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E2ETests;

[Parallelizable(ParallelScope.Self)]
[TestFixture]
public class MicrosoftLoginTests : PageTest
{

    [SetUp]
    public async void Init()
    {

    }

    [Test]
    public async Task Test1()
    {
        await Page.GotoAsync("https://lucaserver.space/shogi", new PageGotoOptions { WaitUntil = WaitUntilState.NetworkIdle });
        var loginButton = Page.GetByRole(AriaRole.Button, new PageGetByRoleOptions { Name = "Log in" });
        await loginButton.ClickAsync();
        //await Page.WaitForURLAsync()
    }
}